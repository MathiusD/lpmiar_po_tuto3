import collections.*;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestListeCirculaire {

    iListeCirculaire<Double> liste;

    @Test
    public void testTaille10_1() {
        liste = new ListeCirculaire<>(10);
        assertEquals(0, liste.tailleCourante());
    }

    @Test
    public void testTaille10_2() {
        liste = new ListeCirculaire<>(10);
        assertEquals(10, liste.tailleMaximum());
    }

    @Test
    public void testTaille0_2() {
        liste = new ListeCirculaire<>(0);
        assertEquals(0, liste.tailleMaximum());
    }

    @Test
    public void testTaille0_1() {
        liste = new ListeCirculaire<>(0);
        assertEquals(0, liste.tailleCourante());
    }

    @Test
    public void testTailleMax0_insererException() {
        liste = new ListeCirculaire<>(0);
        assertThrows(PlusDePlaceException.class,
                () -> liste.insererElement(2.23));
    }

    @Test
    public void testTailleMax1_insererException_1()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(1);
        liste.insererElement(45.87);
        assertThrows(PlusDePlaceException.class,
                () -> liste.insererElement(3.09));
    }

    @Test
    public void testTailleMax2_insererException1()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(2);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        assertThrows(PlusDePlaceException.class,
                () -> liste.insererElement(-12.0));
    }

    @Test
    public void testTailleMax4_insererException_2()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(4);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        assertThrows(PlusDePlaceException.class,
                () -> liste.insererElement(2.23));
    }

    @Test
    public void testTailleMax0_inserer_contientPas() {
        liste = new ListeCirculaire<>(0);
        assertFalse(liste.contient(26.6789));
    }

    @Test
    public void testTailleMax1_inserer_1()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(1);
        liste.insererElement(3.09);
        assertEquals(1, liste.tailleCourante());
    }

    @Test
    public void testTailleMax1_inserer_2()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(1);
        liste.insererElement(3.09);
        assertEquals(1, liste.tailleMaximum());
    }

    @Test
    public void testTailleMax1_inserer_3()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(1);
        liste.insererElement(3.09);
        assertFalse(liste.contient(26.6789));
    }

    @Test
    public void testTailleMax1_inserer_4()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(1);
        liste.insererElement(3.09);
        assertTrue(liste.contient(3.09));
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_1()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertTrue(liste.contient(3.09));
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_2()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertFalse(liste.contient(26.6789));
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_3()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(3, liste.tailleCourante());
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_4()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(5, liste.tailleMaximum());
    }


    @Test
    public void testTailleMax5_inserer_1()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertTrue(liste.contient(45.87));
    }

    @Test
    public void testTailleMax5_inserer_2()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertTrue(liste.contient(2.23));
    }

    @Test
    public void testTailleMax5_inserer_3()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertFalse(liste.contient(26.6789));
    }

    @Test
    public void testTailleMax5_inserer_4()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(5, liste.tailleCourante());
    }

    @Test
    public void testTailleMax5_inserer_5()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(5, liste.tailleMaximum());
    }


    @Test
    public void testTailleMax5_vide_1element() {
        liste = new ListeCirculaire<>(5);
        assertThrows(ListeVideException.class,
                () -> liste.donneElement(1));
    }

    @Test
    public void testTailleMax5_vide_3element() {
        liste = new ListeCirculaire<>(5);
        assertThrows(ListeVideException.class,
                () -> liste.donneElement(3));
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_element_1()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(45.87, liste.donneElement(1));
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_element_2()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(-12.0, liste.donneElement(3));
    }

    @Test
    public void testTailleMax5_pasPleine_inserer_element_3()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(3.09, liste.donneElement(5));
    }

    @Test
    public void testTailleMax5_inserer_elementInvalide0()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertThrows(PositionInvalideException.class,
                () -> liste.donneElement(0));
    }

    @Test
    public void testTailleMax5_inserer_elementInvalide10()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertThrows(PositionInvalideException.class,
                () -> liste.donneElement(-10));
    }

    @Test
    public void testTailleMax5_inserer_1element()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(45.87, liste.donneElement(1));
    }

    @Test
    public void testTailleMax5_inserer_3element()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(-12.0, liste.donneElement(3));
    }

    @Test
    public void testTailleMax5_inserer_5element()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(2.23, liste.donneElement(5));
    }

    @Test
    public void testTailleMax5_inserer_99element()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(13.56789001234, liste.donneElement(99));
    }

    @Test
    public void testTailleMax5_inserer_10000element()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(2.23, liste.donneElement(10000));
    }

    @Test
    public void testTailleMax5_inserer309_element5()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(3.09);
        assertEquals(3.09, liste.donneElement(5));
    }

    @Test
    public void testTailleMax5_inserer309_element10000()
            throws PlusDePlaceException, ListeVideException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(3.09);
        assertEquals(3.09, liste.donneElement(10000));
    }

    @Test
    public void testTailleMax5_vide_suivantDe() {
        liste = new ListeCirculaire<>(5);
        assertThrows(ElementNonPresentException.class,
                () -> liste.suivantDe(-12.0));
    }

    @Test
    public void testTailleMax5_pasPleine_SuivantElementPasLa()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertThrows(ElementNonPresentException.class,
                () -> liste.suivantDe(-3.1415));
    }

    @Test
    public void testTailleMax5_pasPleine_Suivant309()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(3.09);
        assertEquals(3.09, liste.suivantDe(3.09));
    }

    @Test
    public void testTailleMax5_pasPleine_Suivant45()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(3.09, liste.suivantDe(45.87));
    }

    @Test
    public void testTailleMax5_pasPleine_Suivant3()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(-12.0, liste.suivantDe(3.09));
    }

    @Test
    public void testTailleMax5_pasPleine_Suivant12()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        assertEquals(45.87, liste.suivantDe(-12.0));
    }

    @Test
    public void testTailleMax5_Suivant45()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(3.09, liste.suivantDe(45.87));
    }

    @Test
    public void testTailleMax5_Suivant3()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(-12.0, liste.suivantDe(3.09));
    }

    @Test
    public void testTailleMax5_Suivant12()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(13.56789001234, liste.suivantDe(-12.0));
    }

    @Test
    public void testTailleMax5_Suivant2()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertEquals(45.87, liste.suivantDe(2.23));
    }


    @Test
    public void testTailleMax5_pasPleine_iteratorHasNext()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        Iterator<Double> ite = liste.iterator();
        for (int i = 0; i < 1000000; i++) {
            assertTrue(ite.hasNext());
            ite.next();
        }
    }

    @Test
    public void testTailleMax5_pasPleine_iteratorNextElement()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        Double[] attendu = new Double[3];
        attendu[0] = 45.87;
        attendu[1] = 3.09;
        attendu[2] = -12.0;
        Iterator<Double> ite = liste.iterator();
        for (int i = 0; i < 1000000; i++) {
            assertEquals(attendu[i % attendu.length], ite.next());
        }
    }

    @Test
    public void testTailleMax5_iteratorHasNext()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        Iterator<Double> ite = liste.iterator();
        for (int i = 0; i < 1000000; i++) {
            assertTrue(ite.hasNext());
            ite.next();
        }
    }

    @Test
    public void testTailleMax5_iteratorNextElement()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        Double[] attendu = new Double[5];
        attendu[0] = 45.87;
        attendu[1] = 3.09;
        attendu[2] = -12.0;
        attendu[3] = 13.56789001234;
        attendu[4] = 2.23;
        Iterator<Double> ite = liste.iterator();
        for (int i = 0; i < 1000000; i++) {
            assertEquals(attendu[i % attendu.length], ite.next());
        }
    }

    @Test
    public void testTailleMax5_vide_lister_exception() {
        liste = new ListeCirculaire<>(5);
        assertThrows(ElementNonPresentException.class,
                () -> liste.lister(45.87));
    }

    @Test
    public void testTailleMax5_lister_exception()
            throws PlusDePlaceException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        assertThrows(ElementNonPresentException.class,
                () -> liste.lister(-3.14151678));
    }

    @Test
    public void testTailleMax5_lister45()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        List<Double> attendu = new LinkedList<>();
        attendu.add(45.87);
        attendu.add(3.09);
        attendu.add(-12.0);
        attendu.add(13.56789001234);
        attendu.add(2.23);
        assertIterableEquals(attendu, liste.lister(45.87));
    }

    @Test
    public void testTailleMax5_lister2()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        List<Double> attendu = new LinkedList<>();
        attendu.add(2.23);
        attendu.add(45.87);
        attendu.add(3.09);
        attendu.add(-12.0);
        attendu.add(13.56789001234);
        assertIterableEquals(attendu, liste.lister(2.23));
    }

    @Test
    public void testTailleMax5_lister12()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        List<Double> attendu = new LinkedList<>();
        attendu.add(-12.0);
        attendu.add(13.56789001234);
        attendu.add(2.23);
        attendu.add(45.87);
        attendu.add(3.09);
        assertIterableEquals(attendu, liste.lister(-12.0));
    }

    @Test
    public void testlisteTriee()
            throws PlusDePlaceException, ElementNonPresentException, PositionInvalideException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        List<Double> attendu = new LinkedList<>();
        attendu.add(-12.0);
        attendu.add(2.23);
        attendu.add(3.09);
        attendu.add(13.56789001234);
        attendu.add(45.87);
        assertIterableEquals(attendu, liste.trier());
    }

    @Test
    public void testlisteTrieenonModif()
            throws ListeVideException, PlusDePlaceException, PositionInvalideException, ElementNonPresentException {
        liste = new ListeCirculaire<>(5);
        liste.insererElement(45.87);
        liste.insererElement(3.09);
        liste.insererElement(-12.0);
        liste.insererElement(13.56789001234);
        liste.insererElement(2.23);
        liste.trier();
        assertEquals(-12.0, liste.donneElement(3));
    }
}
