package collections;

import java.util.Iterator;
import java.util.List;

/**
 * Défini une liste circulaire d'éléments.
 * <p>
 * Une liste circulaire est une "sorte de" liste qui comporte une caractéristique
 * supplémentaire pour le déplacement dans la file : "elle n'a pas de fin".
 * C'est-à-dire que dans une liste circulaire, nous n'arriverons jamais à une
 * position depuis laquelle nous ne pourrons plus nous déplacer.
 * En arrivant au dernier élément, le déplacement recommencera au premier élément.
 * En bref, il s'agit d'une rotation.
 *
 * @param <E>
 */

public interface iListeCirculaire<E extends Comparable<? super E>> extends Iterable<E> {

    /**
     * donne la taille courante de la liste circulaire, c'est-à-dire le nombre d'éléments
     * contenus à l'instant par la liste.
     *
     * @return la taille actuelle de la liste circulaire
     */
    int tailleCourante();

    /**
     * donne la taille maximum de la liste circulaire, c'est-à-dire le nombre d'éléments
     * maximum que peut contenir la liste.
     *
     * @return la taille maximale que peut atteindre la liste circulaire
     */
    int tailleMaximum();

    /**
     * indique si l'élément considéré est dans la file circulaire
     *
     * @param elementRecherche l'élément à considérer
     * @return true si l'élément est présent ; false sinon
     */
    boolean contient(E elementRecherche);

    /**
     * insère dans la liste circulaire un nouvel élément à la suite de éléments déjà insérer
     *
     * @param elementNouveau l'élément à insérer.
     * @throws PlusDePlaceException la taille maximale est atteinte
     */
    void insererElement(E elementNouveau)
            throws PlusDePlaceException;

    /**
     * donne le n-ième élément de la liste circulaire
     *
     * @param n la position dans la liste du n-ième l'élément à consulter
     *          (on peut être ammené à faire plusieurs tours dans la liste pour atteindre n).
     * @return l'élément en n-ième position dans la liste
     * @throws ListeVideException        si la liste est vide
     * @throws PositionInvalideException si la position indiquée est incorrecte
     */
    E donneElement(int n)
            throws ListeVideException, PositionInvalideException;

    /**
     * donne l'élément situé après l'élément considéré, dans la liste circulaire.
     *
     * @param elementCherche l'élément après celui considéré
     * @return l'élément situé après celui considéré
     * @throws ElementNonPresentException si l'élément considéré n'est pas présent dans la liste
     */
    E suivantDe(E elementCherche) throws ElementNonPresentException;

    /**
     * liste tous les élements de la liste (un unique tour dans la liste) en commençant
     * par l'élément considéré.
     *
     * @param elementCherche l'élément à considérer
     * @return la liste de tous les élements en commençant par l'élément considéré
     * @throws ElementNonPresentException
     */
    List<E> lister(E elementCherche) throws ElementNonPresentException;

    /**
     * donne une liste triée (ordre naturel) de tous les élements de la liste circulaire
     *
     * @return la liste triée tous les élements
     */
    List<E> trier();


    /**
     * donne un iterator sur la liste circulaire qui bouclera infinement.
     * utilise les méthodes donneElement(i) et suivantDe(element).
     *
     * @return un iterator
     */
    @Override
    default Iterator<E> iterator() {
        return new Iterator<E>() {
            E courant = null;

            {
                try {
                    courant = donneElement(1);
                } catch (ListeVideException e) {
                    courant = null;
                } catch (PositionInvalideException e) {
                    // impossible
                }
            }

            @Override
            public boolean hasNext() {
                return (courant != null);
            }

            @Override
            public E next() {
                E precedent = courant;
                try {
                    courant = suivantDe(courant);
                } catch (ElementNonPresentException e) {
                    // impossible
                }
                return precedent;
            }
        };
    }
}
