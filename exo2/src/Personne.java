import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class Personne {

    private String nom;
    private String prenom;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    @Override
    public boolean equals(Object o) {
        try {
            if (this.prenom.compareToIgnoreCase((String) o.getClass().getMethod("getPrenom").invoke(o, null)) == 0)
                if (this.nom.compareToIgnoreCase((String) o.getClass().getMethod("getNom").invoke(o, null)) == 0)
                    return true;
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {

        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%c. %c%s", this.prenom.toUpperCase().charAt(0), this.nom.toUpperCase().charAt(0), this.nom.substring(1).toLowerCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }
}
