import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;

import java.util.*;

public class RepertoireTel implements iRepertoireTel, iRandomUser {

    private Map<Personne, List<NumeroTel>> annuaire;

    public RepertoireTel() {
        this.annuaire = new LinkedHashMap<>();
    }

    @Override
    public void ajouterContact(Personne nouveauContact, NumeroTel numero) throws ContactDejaPresentException{
        if (!this.annuaire.containsKey(nouveauContact)) {
            List<NumeroTel> numeros = new ArrayList<>();
            numeros.add(numero);
            this.annuaire.put(nouveauContact, numeros);
        } else
            throw new ContactDejaPresentException(String.format("Personne %s is already in RepertoireTel", nouveauContact));
    }

    @Override
    public void ajouterNumero(Personne contactExistant, NumeroTel numero)
            throws ContactInconnuException, NumeroDejaPresentException {
        if (this.annuaire.containsKey(contactExistant))
            if (!this.annuaire.get(contactExistant).contains(numero))
                this.annuaire.get(contactExistant).add(numero);
            else
                throw new NumeroDejaPresentException(String.format("Numero %s is already in RepertoireTel", numero));
        else
            throw new ContactInconnuException(String.format("Personne %s isn't in RepertoireTel", contactExistant));
    }

    @Override
    public List<Personne> listerContacts() {
        return new ArrayList<>(this.annuaire.keySet());
    }

    @Override
    public List<NumeroTel> listerNumeros(Personne contactExistant) throws ContactInconnuException {
        if (!this.annuaire.containsKey(contactExistant))
            throw new ContactInconnuException(String.format("Personne %s isn't in RepertoireTel", contactExistant));
        return new ArrayList<>(this.annuaire.get(contactExistant));
    }

    @Override
    public void supprimerNumero(Personne contactExistant, NumeroTel numero) throws ContactInconnuException, NumeroInconnuException{
        if (this.annuaire.containsKey(contactExistant))
            if (this.annuaire.get(contactExistant).contains(numero)) {
                this.annuaire.get(contactExistant).remove(numero);
                if (this.annuaire.get(contactExistant).size() == 0)
                    this.annuaire.remove(contactExistant);
            }
            else
                throw new NumeroInconnuException(String.format("Numero %s is already in RepertoireTel", numero));
        else
            throw new ContactInconnuException(String.format("Personne %s isn't in RepertoireTel", contactExistant));
    }

    @Override
    public void fromRandomUser(int nbUsers) throws TelMauvaisFormatException, ContactDejaPresentException, ContactInconnuException, NumeroDejaPresentException {
        int failure = nbUsers;
        HttpResponse<String> request =  Unirest.get(String.format("https://randomuser.me/api/?nat=fr&results=%s", nbUsers)).asString();
        if (request.getStatus() == 200) {
            JSONObject raw = new JsonNode(request.getBody()).getObject();
            Set<String> rawKeys = raw.keySet();
            if (rawKeys.contains("results")) {
                JSONArray rawData = raw.getJSONArray("results");
                if (!rawData.isEmpty()) {
                    for (int index = 0; index < rawData.length(); index++) {
                        JSONObject json = rawData.getJSONObject(index);
                        Set<String> keys = json.keySet();
                        if (keys.contains("name") && keys.contains("phone") && keys.contains("cell")) {
                            JSONObject name = json.getJSONObject("name");
                            Set<String> nameKeys = name.keySet();
                            try {
                                NumeroTel phone = NumeroTel.fabrique(json.getString("phone"));
                                NumeroTel cell = NumeroTel.fabrique(json.getString("cell"));
                                if (nameKeys.contains("first") && nameKeys.contains("last")) {
                                    Personne personne = new Personne(name.getString("last"), name.getString("first"));
                                    if (this.listerContacts().contains(personne))
                                        this.ajouterNumero(personne, phone);
                                    else
                                        this.ajouterContact(personne, phone);
                                    this.ajouterNumero(personne, cell);
                                    failure -= 1;
                                }
                            } catch (TelMauvaisFormatException exception) {

                            }
                        }
                    }
                }
            }
        }
        if (failure != 0)
            this.fromRandomUser(failure);
    }
}
