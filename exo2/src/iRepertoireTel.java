import java.util.List;

public interface iRepertoireTel {
    void ajouterContact(Personne nouveauContact, NumeroTel numero)
            throws ContactDejaPresentException;

    void ajouterNumero(Personne contactExistant, NumeroTel numero)
            throws ContactInconnuException, NumeroDejaPresentException;

    List<NumeroTel> listerNumeros(Personne contactExistant) throws ContactInconnuException;

    void supprimerNumero(Personne contactExistant, NumeroTel numero)
            throws ContactInconnuException, NumeroInconnuException;

    List<Personne> listerContacts();
}
