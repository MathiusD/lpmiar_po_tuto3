public class NumeroInconnuException extends Exception {
    public NumeroInconnuException(String s) {
        super(s);
    }
}
