import java.util.Objects;
import java.util.regex.Pattern;

public class NumeroTel {

    private String numero;

    private NumeroTel(String num) {
        this.numero = num;
    }

    /**
     * verifie que le numero passé en paramètre est correctement formaté.
     * Accepte uniquement les numéros de tel commençant par 0, composé de 10 digits,
     * avec (pas obligatoire) tous les deux digits, des espaces, des '-' ou des '.'
     *
     * @param num la chaine de caractere à valider
     * @return un numero de teléphone
     * @throws TelMauvaisFormatException si le format de la chaine est mauvais
     */
    public static NumeroTel fabrique(String num) throws TelMauvaisFormatException {
        if (Pattern.matches("((0[0-9])((\\.|-| )?([0-9]){2}){4})", num))
            return new NumeroTel(num);
        else
            throw new TelMauvaisFormatException(String.format("This string %s dosen't match with phone number.", num));
    }

    /**
     * Deux numeros sont égaux quelque soit leur format d'entrée, s'ils ont exactement les mêmes 10 digits
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass() == this.getClass())
            return this.hashCode() == o.hashCode();
        return false;
    }

    @Override
    public String toString() {
        String out = "";
        for (String digit :this.numero.split("( |\\.|-)", this.numero.length()))
            out = String.format("%s%s", out, digit);
        return out;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.toString());
    }
}