public class ContactDejaPresentException extends Exception {
    public ContactDejaPresentException(String s) {
        super(s);
    }
}
