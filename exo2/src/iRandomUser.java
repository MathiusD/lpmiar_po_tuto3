public interface iRandomUser {

    void fromRandomUser(int nbUsers) throws TelMauvaisFormatException, ContactDejaPresentException, ContactInconnuException, NumeroDejaPresentException;
}
