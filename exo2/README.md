# exo2 : utilisation d'une API JSON

L'objectif de cet exercice va consister à alimenter 
le repertoire téléphonique implémenté `tuto2 > exo 6` à partir de personnes 
et de numéros de téléphones récupéré via l'API _RandomUser_ : https://randomuser.me


_RandomUser_ s'utilise en requetant l'API. Par exemple : https://randomuser.me/api/?nat=fr
généère un JSON avec toute les informations d'une personnne. La précision `nat=fr` permet d'indiquer que l'on veut récupérer des contacts français,
et donc d'obtenir des numéros de téléphone correct.
Deux téléphones sont indiqués pour chaque personne générée : `phone` et `cell`

Commencez par récupérer le code que vous avez écrit pour les classes
`NumeroTel` et `RepertoireTel`.

Si nécessaire, ajoutez l'interface `iRandomUser` à `RepertoireTel`, et implémentez 
la méthode fromRandomUser(int nbUsers) qui remplit le répertoire à partir de
personnes générées par _RandomUser_.
