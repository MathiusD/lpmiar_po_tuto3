import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRandomUser {

    RepertoireTel repertoire;

    @BeforeEach
    public void init() {
        repertoire = new RepertoireTel();
    }

    @Test
    public void randomUser1() throws TelMauvaisFormatException,
            ContactInconnuException, ContactDejaPresentException,
            NumeroDejaPresentException {
        repertoire.fromRandomUser(1);
        List<Personne> contacts = repertoire.listerContacts();
        System.out.println(contacts);
        assertEquals(1, contacts.size());
        List<NumeroTel> numeros = repertoire.listerNumeros(contacts.get(0));
        System.out.println(numeros);
        assertEquals(2, numeros.size());
    }

    @Test
    public void randomUser10() throws TelMauvaisFormatException,
            ContactInconnuException, ContactDejaPresentException,
            NumeroDejaPresentException {
        repertoire.fromRandomUser(10);
        List<Personne> contacts = repertoire.listerContacts();
        System.out.println(contacts);
        assertEquals(10, contacts.size());
        for(Personne p : contacts) {
            List<NumeroTel> numeros = repertoire.listerNumeros(p);
            assertEquals(2, numeros.size());
        }
    }

    @Test
    public void randomUser99() throws TelMauvaisFormatException,
            ContactInconnuException, ContactDejaPresentException,
            NumeroDejaPresentException {
        repertoire.fromRandomUser(99);
        List<Personne> contacts = repertoire.listerContacts();
        System.out.println(contacts);
        assertEquals(99, contacts.size());
        for(Personne p : contacts) {
            List<NumeroTel> numeros = repertoire.listerNumeros(p);
            assertEquals(2, numeros.size());
        }
    }
}
