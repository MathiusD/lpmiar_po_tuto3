import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class TestRepTel {

    Personne tony;
    Personne bruce;
    RepertoireTel repertoire;

    @BeforeEach
    public void init() {
        repertoire = new RepertoireTel();
        tony = new Personne("Stark", "Tony");
        bruce = new Personne("Wayne", "Bruce");
    }

    @Test
    public void ajoutNouveauContact() {
        assertDoesNotThrow(() -> repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789")));
    }

    @Test
    public void ajoutNouveauContact2()
            throws TelMauvaisFormatException, ContactDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        assertDoesNotThrow(() -> repertoire.ajouterContact(bruce, NumeroTel.fabrique("06.34.23.56.56")));
    }

    @Test
    public void ajoutNouveauContact_ex3()
            throws TelMauvaisFormatException, ContactDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterContact(bruce, NumeroTel.fabrique("06.34.23.56.56"));
        assertThrows(ContactDejaPresentException.class,
                () -> repertoire.ajouterContact(tony, NumeroTel.fabrique("09-23-23-23-45")));
    }

    @Test
    public void ajoutNouveauNumero1()
            throws TelMauvaisFormatException, ContactDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterContact(bruce, NumeroTel.fabrique("06.34.23.56.56"));
        assertDoesNotThrow(() -> repertoire.ajouterNumero(tony, NumeroTel.fabrique("09-23-23-23-45")));
    }

    @Test
    public void ajoutNouveauNumero_ex2()
            throws TelMauvaisFormatException,
            ContactDejaPresentException, ContactInconnuException, NumeroDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterNumero(tony, NumeroTel.fabrique("09-23-23-23-45"));
        assertThrows(ContactInconnuException.class,
                () -> repertoire.ajouterNumero(bruce, NumeroTel.fabrique("06.34.23.56.56")));
    }

    @Test
    public void ajoutNouveauNumero_ex1()
            throws TelMauvaisFormatException, ContactDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterContact(bruce, NumeroTel.fabrique("06.34.23.56.56"));
        assertThrows(NumeroDejaPresentException.class,
                () -> repertoire.ajouterNumero(tony, NumeroTel.fabrique("0123456789")));
    }

    @Test
    public void ajoutNouveauNumero_ex3()
            throws TelMauvaisFormatException, ContactDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterContact(bruce, NumeroTel.fabrique("06.34.23.56.56"));
        assertThrows(NumeroDejaPresentException.class,
                () -> repertoire.ajouterNumero(tony, NumeroTel.fabrique("01.23.45.67.89")));
    }


    @Test
    public void listerNumeros()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        assertAll(
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num1, num3}),
                        repertoire.listerNumeros(tony)),
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num2}),
                        repertoire.listerNumeros(bruce))
        );
    }

    @Test
    public void listerNumeros_ex()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterNumero(tony, num3);
        assertAll(
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num1, num3}), repertoire.listerNumeros(tony)),
                () -> assertThrows(ContactInconnuException.class, () -> repertoire.listerNumeros(bruce))
        );
    }

    @Test
    public void listerNumeros_ex2() {
        assertAll(
                () -> assertThrows(ContactInconnuException.class, () -> repertoire.listerNumeros(tony)),
                () -> assertThrows(ContactInconnuException.class, () -> repertoire.listerNumeros(bruce))
        );
    }

    @Test
    public void listerContacts1()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterContact(bruce, NumeroTel.fabrique("06.34.23.56.56"));
        repertoire.ajouterNumero(tony, NumeroTel.fabrique("09-23-23-23-45"));
        assertIterableEquals(Arrays.asList(new Personne[]{tony, bruce}), repertoire.listerContacts());
    }

    @Test
    public void listerContacts2()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException {
        repertoire.ajouterContact(tony, NumeroTel.fabrique("0123456789"));
        repertoire.ajouterNumero(tony, NumeroTel.fabrique("09-23-23-23-45"));
        assertIterableEquals(Arrays.asList(new Personne[]{tony}), repertoire.listerContacts());
    }

    @Test
    public void listerContacts3() {
        assertEquals(Collections.EMPTY_LIST, repertoire.listerContacts());
    }

    @Test
    public void supprimer()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException, NumeroInconnuException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        //
        repertoire.supprimerNumero(tony, num1);
        //
        assertAll(
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num3}),
                        repertoire.listerNumeros(tony)),
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num2}),
                        repertoire.listerNumeros(bruce)),
                () -> assertIterableEquals(Arrays.asList(new Personne[]{tony, bruce}), repertoire.listerContacts())
        );
    }

    @Test
    public void supprimer2()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException, NumeroInconnuException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        //
        repertoire.supprimerNumero(tony, num3);
        //
        assertAll(
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num1}),
                        repertoire.listerNumeros(tony)),
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num2}),
                        repertoire.listerNumeros(bruce)),
                () -> assertIterableEquals(Arrays.asList(new Personne[]{tony, bruce}), repertoire.listerContacts())
        );
    }

    @Test
    public void supprimer3()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException, NumeroInconnuException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        //
        repertoire.supprimerNumero(bruce, num2);
        //
        assertAll(
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num1, num3}),
                        repertoire.listerNumeros(tony)),
                () -> assertIterableEquals(Arrays.asList(new Personne[]{tony}), repertoire.listerContacts())
        );
    }

    @Test
    public void supprimer4()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException, NumeroInconnuException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        //
        repertoire.supprimerNumero(tony, num3);
        repertoire.supprimerNumero(tony, num1);
        //
        assertAll(
                () -> assertIterableEquals(Arrays.asList(new NumeroTel[]{num2}),
                        repertoire.listerNumeros(bruce)),
                () -> assertIterableEquals(Arrays.asList(new Personne[]{bruce}), repertoire.listerContacts())
        );
    }

    @Test
    public void supprimer_ex1()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException, NumeroInconnuException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        //
        assertThrows(NumeroInconnuException.class,
                () -> repertoire.supprimerNumero(tony, NumeroTel.fabrique("05.34.67.67.67")));
    }

    @Test
    public void supprimer_ex2()
            throws TelMauvaisFormatException, ContactDejaPresentException,
            ContactInconnuException, NumeroDejaPresentException, NumeroInconnuException {
        NumeroTel num1 = NumeroTel.fabrique("0123456789");
        NumeroTel num2 = NumeroTel.fabrique("06.34.23.56.56");
        NumeroTel num3 = NumeroTel.fabrique("09-23-23-23-45");
        repertoire.ajouterContact(tony, num1);
        repertoire.ajouterContact(bruce, num2);
        repertoire.ajouterNumero(tony, num3);
        //
        assertThrows(NumeroInconnuException.class,
                () -> repertoire.supprimerNumero(tony, num2));
    }
}
