import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestNumeroTel {

    @Test
    public void fabrique0() {
        assertDoesNotThrow(() -> NumeroTel.fabrique("07.69.46.33.12"));
    }

    @Test
    public void fabrique1() {
        assertDoesNotThrow(() -> NumeroTel.fabrique("0769463312"));
    }

    @Test
    public void fabrique2() {
        assertDoesNotThrow(() -> NumeroTel.fabrique("07-69-46-33-12"));
    }

    @Test
    public void fabrique3() {
        assertDoesNotThrow(() -> NumeroTel.fabrique("07 69 46 33 12"));
    }

    @Test
    public void fabrique_bad0() {
        assertThrows(TelMauvaisFormatException.class,
                () -> NumeroTel.fabrique(""));
    }

    @Test
    public void fabrique_bad1() {
        assertThrows(TelMauvaisFormatException.class,
                () -> NumeroTel.fabrique("azertyezrerr"));
    }

    @Test
    public void fabrique_bad3() {
        assertThrows(TelMauvaisFormatException.class,
                () -> NumeroTel.fabrique("1234567890"));
    }

    @Test
    public void fabrique_bad4() {
        assertThrows(TelMauvaisFormatException.class,
                () -> NumeroTel.fabrique("0.123456789"));
    }

    @Test
    public void fabrique_bad5() {
        assertThrows(TelMauvaisFormatException.class,
                () -> NumeroTel.fabrique("012.3456.789"));
    }

    @Test
    public void fabrique_bad6() {
        assertThrows(TelMauvaisFormatException.class,
                () -> NumeroTel.fabrique("012 3456 789"));
    }

    @Test
    public void egalite0() throws TelMauvaisFormatException {
        NumeroTel tel1 = NumeroTel.fabrique("07-69-46-33-12");
        NumeroTel tel2 = NumeroTel.fabrique("07-69-46-33-12");
        assertTrue(tel1.equals(tel2));
    }

    @Test
    public void egalite1() throws TelMauvaisFormatException {
        NumeroTel tel1 = NumeroTel.fabrique("07-69-46-33-12");
        NumeroTel tel2 = NumeroTel.fabrique("07 69 46 33 12");
        assertTrue(tel1.equals(tel2));
    }

    @Test
    public void egalite2() throws TelMauvaisFormatException {
        NumeroTel tel1 = NumeroTel.fabrique("07.69.46.33.12");
        NumeroTel tel2 = NumeroTel.fabrique("07 69 46 33 12");
        assertTrue(tel1.equals(tel2));
    }

    @Test
    public void egalite3() throws TelMauvaisFormatException {
        NumeroTel tel1 = NumeroTel.fabrique("07.69.46.33.12");
        NumeroTel tel2 = NumeroTel.fabrique("07-69-46-33-12");
        assertTrue(tel1.equals(tel2));
    }

    @Test
    public void nonEgalite0() throws TelMauvaisFormatException {
        NumeroTel tel1 = NumeroTel.fabrique("07-69-46-33-12");
        NumeroTel tel2 = NumeroTel.fabrique("08-69-46-33-12");
        assertFalse(tel1.equals(tel2));
    }

}
