# exo 2 : utilisation d'une API JSON

On va "jouer" avec l'API JSON "Punk API" :
https://punkapi.com/documentation/v2

Pour exécuter des requêtes HTTP GET/POST, on utilisera la librairie `Unirest-java` :
http://kong.github.io/unirest-java

(normalement elle est déjà installée dans `lib/`).

Regardez les exemples donnés dans `UsageUnirest`.

Complétez la classe `Biere`.

