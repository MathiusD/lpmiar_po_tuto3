import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.junit.jupiter.api.Test;

public class UsageUnirest {


    @Test
    public void exemple_simpleGet() {
        HttpResponse<String> reponse = Unirest.get("https://www.perdu.com").asString();
        System.out.println(reponse.getBody());
    }


    @Test
    public void exemple_autreGet() {
        System.out.println(Unirest
                .get("https://httpbin.org/get")
                .queryString("fruit", "apple")
                .queryString("droid", "R2D2")
                .asString().getBody());
        // NB : http://httpbin.org/ renvoie simplement les informations qu'on lui envoie
    }

    @Test
    public void exemple_simplePost() {
        HttpResponse<String> reponse =
                Unirest
                        .post("https://httpbin.org/post")
                        .field("fruit", "apple")
                        .field("droid", "R2D2")
                        .asString();
        // NB : http://httpbin.org/ renvoie simplement les informations qu'on lui envoie
        System.out.println(reponse.getBody());
    }

    @Test
    public void exemple_simpleJson() {
        JSONObject json1 =
                Unirest.post("https://httpbin.org/post")
                        .field("fruit", "apple")
                        .field("droid", "R2D2")
                        .asJson()
                        .getBody()
                        .getObject();

        // NB : http://httpbin.org/ renvoie simplement les informations qu'on lui envoie
        JSONObject json3 = json1.getJSONObject("form");
        String droid = json3.getString("droid");
        String fruit = json3.getString("fruit");
        System.out.println("Le JSON renvoyé contient : " + droid + " et " + fruit);
    }


}
