import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestBiere {

    @Test
    public void biere1() {
        String result = Biere.donneJSON(1);
        assertTrue(result.contains("Buzz") && result.contains("A Real Bitter Experience."));
    }

    @Test
    public void biere100() {
        String result = Biere.donneJSON(100);
        assertTrue(result.contains("Elvis Juice V2.0 - Prototype Challenge"));
    }

    @Test
    public void biere666() {
        String result = Biere.donneJSON(666);
        assertTrue(result.contains("\"No beer found that matches the ID 666\""));
    }

    @Test
    public void donneBiere78() {
        Biere b = new Biere("AB:18",
                "AB:18 began life as an Imperial Brown Ale before we bombarded it with Scottish tayberries and Purple raspberries and locked it away in rum barrels for two years, infusing rich dark fruit flavours and oaky warmth into the mix.",
                11.8,
                "https://images.punkapi.com/v2/78.png");
        assertEquals(b, Biere.donneBiere(78));
    }

    @Test
    public void donneBiere97() {
        Biere b = new Biere("Bowman's Beard - B-Sides",
                "Ice-distilled double barley wine brewed by Chris from Stone Brewing Co. (see Sunmaid Stout) and BrewDog's own brewers. As robust, resinous and badass as its eponymic beard.",
                18.3,
                "https://images.punkapi.com/v2/keg.png");
        assertEquals(b, Biere.donneBiere(97));
    }

    @Test
    public void donneBiere66() {
        Biere b = new Biere("Bowman's Beard - B-Sides",
                "Ice-distilled double barley wine brewed by Chris from Stone Brewing Co. (see Sunmaid Stout) and BrewDog's own brewers. As robust, resinous and badass as its eponymic beard.",
                18.3,
                "https://images.punkapi.com/v2/keg.png");
        Biere b_result = Biere.donneBiere(66);
        assertNotNull(b_result);
        assertNotEquals(b, b_result);
    }

    @Test
    public void donneBieres_10_56() {
        List<Biere> bieres = Biere.donneBieres(10, 5.6);
        assertAll(
                () -> assertNotNull(bieres),
                () -> assertTrue(bieres.size() <= 10)
        );
        for (Biere b : bieres) {
            assertTrue(b.abv() <= 5.6);
        }
    }

    @Test
    public void donneBieres_80_36() {
        List<Biere> bieres = Biere.donneBieres(80, 3.6);
        assertAll(
                () -> assertNotNull(bieres),
                () -> assertTrue(bieres.size() <= 80),
                () -> assertEquals(12, bieres.size())
        );
        for (Biere b : bieres) {
            assertTrue(b.abv() <= 3.6);
        }
    }

    @Test
    public void donneBieres_80_06() {
        List<Biere> bieres = Biere.donneBieres(80, 0.6);
        assertAll(
                () -> assertNotNull(bieres),
                () -> assertTrue(bieres.size() <= 80),
                () -> assertEquals(2, bieres.size())
        );
        for (Biere b : bieres) {
            assertTrue(b.abv() <= 0.6);
        }
    }

    @Test
    public void donneBieres_80_02() {
        List<Biere> bieres = Biere.donneBieres(80, 0.2);
        assertAll(
                () -> assertNotNull(bieres),
                () -> assertTrue(bieres.isEmpty())
        );
    }
}


