import com.google.gson.JsonObject;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.JsonObjectMapper;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Biere {

    private String nom;
    private String desc;
    private double abv;
    private String url;
    private static String uri = "https://api.punkapi.com/v2/beers/";

    public Biere(String nom, String desc, double abv, String url) {
        this.nom = nom;
        this.desc = desc;
        this.abv = abv;
        this.url = url;

    }

    /**
     * retourne la chaine de caractère correspondant au résultat de la requete sur l'API pour l'id considéré
     *
     * @param id id d'une bière
     * @return une chaine de caractère
     */
    public static String donneJSON(int id) {
        String url = String.format("%s%s", Biere.uri, id);
        return Unirest.get(url).asString().getBody();
    }

    /**
     * retourne un objet Bière instancié avec les valeurs récupérées sur l'API considéré
     *
     * @param id d'une bière
     * @return un nouvel objet Biere
     */
    public static Biere donneBiere(int id) {
        return Biere.fromJson(Biere.donneJSON(id));
    }

    public static Biere fromJson(String rawData) {
        JSONObject json = new JsonNode(rawData).getObject();
        if (json == null) {
            JSONArray tab = new JsonNode(rawData).getArray();
            if (!tab.isEmpty())
                json = tab.getJSONObject(0);
        }
        if (json != null) {
            Set<String> keys = json.keySet();
            if (keys.contains("name") && keys.contains("description") && keys.contains("abv") && keys.contains("image_url"))
                return new Biere(
                        json.getString("name"),
                        json.getString("description"),
                        json.getDouble("abv"),
                        json.getString("image_url")
                );
        }
        return null;
    }

    /**
     * donne une liste contenant au maxium 'nbBieres' bieres, dont l'ABV est  <= 'abvLimite'
     *
     * @param nbBieres  le nombre de bières à rechercher
     * @param abvLimite la limite d'ABV pour les bieres
     * @return une liste de bières
     */
    public static List<Biere> donneBieres(int nbBieres, double abvLimite) {
        String url = String.format("%s?abv_lt=%s&per_page=%s", Biere.uri, abvLimite, nbBieres);
        HttpResponse<String> response = Unirest.get(url).asString();
        if (response.getStatus() == 200) {
            List<Biere> bieres = new ArrayList<>();
            for (Object data : new JsonNode(response.getBody()).getArray())
                bieres.add(Biere.fromJson(data.toString()));
            return bieres;
        }
        return null;
    }

    double abv() {
        return abv;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Biere)) return false;

        Biere biere = (Biere) o;

        if (Double.compare(biere.abv, abv) != 0) return false;
        if (nom != null ? !nom.equals(biere.nom) : biere.nom != null) return false;
        if (desc != null ? !desc.equals(biere.desc) : biere.desc != null) return false;
        return url != null ? url.equals(biere.url) : biere.url == null;
    }

    @Override
    public String toString() {
        return this.nom + " (" + this.abv + "°)";
    }


}
