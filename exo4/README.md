# exo 4 : algorithmique
 
_Une expression mathématique simple peut être représenter par un arbre :
 évaluer l'expression correspond alors à parcourir l'arbre correspondant._

**Voir ExempleNoeudExpr.pdf**


Les classes `NoeudValeurEntiere` et `NoeudOperateurEntier` représentent des 
noeuds de l'arbre.

Dans `Test3`, on construit l'arbre correspondant à l'exemple.

Complétez correctement les classes `NoeudValeurEntiere` et `NoeudOperateurEntier`.