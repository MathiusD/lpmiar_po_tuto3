import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

public class Test3 {

    iNoeudExpr<Integer> test;
    iNoeudExpr<Integer> test2;

    @BeforeEach
    void init() throws PasDeFilsException, EvaluationImpossibleException {
        // (2*((10/2)+3))
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "*",
                new NoeudOperateurEntier(
                        new NoeudOperateurEntier(
                                new NoeudValeurEntiere("10"),
                                "/",
                                new NoeudValeurEntiere("2")
                        ),
                        "+",
                        new NoeudValeurEntiere("3")
                )
        );

        test2 = new NoeudOperateurEntier(
                new NoeudOperateurEntier(test, "*", test),
                "-",
                new NoeudOperateurEntier(test, "*", test)
        );
    }

    @Test
    void expr_affiche() {
        assertEquals("(2*((10/2)+3))", test.affiche());
    }

    @Test
    void expr_evalue() {
        assertEquals(16, test.evalue());
    }

    @Test
    void expr_affiche_filsD() throws PasDeFilsException {
        assertEquals("((10/2)+3)", test.donneFilsD().affiche());
    }

    @Test
    void expr_affiche_filsG() throws PasDeFilsException {
        assertEquals("2", test.donneFilsG().affiche());
    }

    @Test
    void expr_nb() {
        assertEquals(4, test.nbValeursNumeriques());
    }

    @Test
    void expr_listeEntiers() {
        Integer[] attendu = {2, 10, 2, 3};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeurs());
    }

    @Test
    void expr_listeEntiersTries() {
        Integer[] attendu = {2, 3, 10};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeursTriees());
    }

    @Test
    void expr_listeOpe() {
        String[] attendu = {"*", "/", "+"};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesOperateurs());
    }

    @Test
    void expr2_affiche() {
        assertEquals("(((2*((10/2)+3))*(2*((10/2)+3)))-((2*((10/2)+3))*(2*((10/2)+3))))", test2.affiche());
    }

    @Test
    void expr2_evalue() {
        assertEquals(0, test2.evalue());
    }

    @Test
    void expr2_nb() {
        assertEquals(16, test2.nbValeursNumeriques());
    }

    @Test
    void expr2_listeEntiers() {
        Integer[] attendu = {2, 10, 2, 3, 2, 10, 2, 3, 2, 10, 2, 3, 2, 10, 2, 3};
        assertIterableEquals(Arrays.asList(attendu), test2.listeDesValeurs());
    }

    @Test
    void expr2_listeEntiersTries() {
        Integer[] attendu = {2, 3, 10};
        assertIterableEquals(Arrays.asList(attendu), test2.listeDesValeursTriees());
    }

    @Test
    void expr2_listeOpe() {
        String[] attendu = {"*", "/", "+", "*", "*", "/", "+", "-", "*", "/", "+", "*", "*", "/", "+"};
        assertIterableEquals(Arrays.asList(attendu), test2.listeDesOperateurs());
    }
}
