import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class Test2 {


    iNoeudExpr<Integer> test;

    @Test
    void test2a3() {
        assertThrows(EvaluationImpossibleException.class,
                () -> test = new NoeudOperateurEntier(
                        new NoeudValeurEntiere("2"),
                        "a",
                        new NoeudValeurEntiere("3")));
    }

    @Test
    void test2percent3() {
        assertThrows(EvaluationImpossibleException.class,
                () -> test = new NoeudOperateurEntier(
                        new NoeudValeurEntiere("2"),
                        "%",
                        new NoeudValeurEntiere("3")));
    }

    @Test
    void test2null3() {
        assertThrows(EvaluationImpossibleException.class,
                () -> test = new NoeudOperateurEntier(
                        new NoeudValeurEntiere("2"),
                        null,
                        new NoeudValeurEntiere("3")));
    }

    @Test
    void test2empty3() {
        assertThrows(EvaluationImpossibleException.class,
                () -> test = new NoeudOperateurEntier(
                        new NoeudValeurEntiere("2"),
                        "",
                        new NoeudValeurEntiere("3")));
    }

    @Test
    void test2plus3() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "+",
                new NoeudValeurEntiere("3"));
        return;
    }

    @Test
    void test3moins2() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "-",
                new NoeudValeurEntiere("2"));
        return;
    }

    @Test
    void test3fois2() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        return;
    }

    @Test
    void test3div2() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "/",
                new NoeudValeurEntiere("2"));
        return;
    }

    @Test
    void test3div() {
        assertThrows(PasDeFilsException.class,
                () -> test = new NoeudOperateurEntier(
                        new NoeudValeurEntiere("3"),
                        "/",
                        null));
    }

    @Test
    void testdiv2() {
        assertThrows(PasDeFilsException.class,
                () -> test = new NoeudOperateurEntier(
                        null,
                        "/",
                        new NoeudValeurEntiere("2")));
    }


    @Test
    void test3moins2_aFilsD() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "-",
                new NoeudValeurEntiere("2"));
        assertTrue(test.aFilsD());
    }

    @Test
    void test3moins2_aFilsG() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "-",
                new NoeudValeurEntiere("2"));
        assertTrue(test.aFilsG());
    }

    @Test
    void test3moins2_donneFilsD() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "-",
                new NoeudValeurEntiere("2"));
        assertEquals(2, test.donneFilsD().evalue());
    }

    @Test
    void test3moins2_donneFilsG() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "-",
                new NoeudValeurEntiere("2"));
        assertEquals(3, test.donneFilsG().evalue());
    }

    @Test
    void test2plus3_evalue() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "+",
                new NoeudValeurEntiere("3"));
        assertEquals(5, test.evalue());
    }

    @Test
    void test3moins2_evalue() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "-",
                new NoeudValeurEntiere("2"));
        assertEquals(1, test.evalue());
    }

    @Test
    void test2moins3_evalue() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "-",
                new NoeudValeurEntiere("3"));
        assertEquals(-1, test.evalue());
    }

    @Test
    void test3fois2_evalue() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        assertEquals(6, test.evalue());
    }

    @Test
    void test3div2_evalue() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "/",
                new NoeudValeurEntiere("2"));
        assertEquals(1, test.evalue());
    }

    @Test
    void test2div3_evalue() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "/",
                new NoeudValeurEntiere("3"));
        assertEquals(0, test.evalue());
    }

    @Test
    void test2moins3_affiche() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "-",
                new NoeudValeurEntiere("3"));
        assertEquals("(2-3)", test.affiche());
    }

    @Test
    void test3fois2_affiche() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        assertEquals("(3*2)", test.affiche());
    }

    @Test
    void test2div3_affiche() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "/",
                new NoeudValeurEntiere("3"));
        assertEquals("(2/3)", test.affiche());
    }

    @Test
    void test2moins3_nb() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "-",
                new NoeudValeurEntiere("3"));
        assertEquals(2, test.nbValeursNumeriques());
    }

    @Test
    void test3fois2_nb() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        assertEquals(2, test.nbValeursNumeriques());
    }

    @Test
    void test2div3_nb() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "/",
                new NoeudValeurEntiere("3"));
        assertEquals(2, test.nbValeursNumeriques());
    }

    @Test
    void test2moins3_listeOpe() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "-",
                new NoeudValeurEntiere("3"));
        String[] attendu = {"-"};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesOperateurs());
    }

    @Test
    void test3fois2_listeOpe() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        String[] attendu = {"*"};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesOperateurs());
    }

    @Test
    void test2div3_listeOpe() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "/",
                new NoeudValeurEntiere("3"));
        String[] attendu = {"/"};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesOperateurs());
    }

    @Test
    void test2moins3_listeEntiers() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "-",
                new NoeudValeurEntiere("3"));
        Integer[] attendu = {2, 3};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeurs());
    }

    @Test
    void test3fois2_listeEntiers() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        Integer[] attendu = {3, 2};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeurs());
    }

    @Test
    void test2div3_listeEntiers() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("5"),
                "/",
                new NoeudValeurEntiere("7"));
        Integer[] attendu = {5, 7};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeurs());
    }

    @Test
    void test2div3_listeEntiersTries() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("5"),
                "/",
                new NoeudValeurEntiere("7"));
        Integer[] attendu = {5, 7};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeursTriees());
    }

    @Test
    void test3fois2_listeEntiersTries() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("3"),
                "*",
                new NoeudValeurEntiere("2"));
        Integer[] attendu = {2, 3};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeursTriees());
    }

    @Test
    void test2fois2_listeEntiersTries() throws PasDeFilsException, EvaluationImpossibleException {
        test = new NoeudOperateurEntier(
                new NoeudValeurEntiere("2"),
                "*",
                new NoeudValeurEntiere("2"));
        Integer[] attendu = {2};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeursTriees());
    }
}
