import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class Test1 {

    iNoeudExpr<Integer> test;

    @Test
    void entier_a() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere("a"));
    }

    @Test
    void entier_null() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere(null));
    }

    @Test
    void entier_plus() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere("+"));
    }

    @Test
    void entier_div() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere("/"));
    }

    @Test
    void entier_Float() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere("3.25"));
    }

    @Test
    void entier_1_float() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere("1.0"));
    }

    @Test
    void entier_1() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        return;
    }

    @Test
    void entier_0_float() {
        assertThrows(EvaluationImpossibleException.class,
                () -> new NoeudValeurEntiere("0.0"));
    }

    @Test
    void entier_0() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("0");
        return;
    }

    @Test
    void entier_99() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("99");
        return;
    }

    @Test
    void entier_evalue_1() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        assertEquals(1, test.evalue());
    }

    @Test
    void entier_evalue_0() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("0");
        assertEquals(0, test.evalue());
    }

    @Test
    void entier_evalue_99() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("99");
        assertEquals(99, test.evalue());
    }

    @Test
    void entier_filsD_2() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("2");
        assertFalse(test.aFilsD());
    }

    @Test
    void entier_filsG_2() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("2");
        assertFalse(test.aFilsG());
    }

    @Test
    void entier_donnefilsD_2() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("2");
        assertThrows(PasDeFilsException.class, () -> test.donneFilsD());
    }

    @Test
    void entier_donnefilsG_2() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("2");
        assertThrows(PasDeFilsException.class, () -> test.donneFilsG());
    }

    @Test
    void entier_1_affiche() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        assertEquals("1", test.affiche());
    }

    @Test
    void entier_1_nb() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        assertEquals(1, test.nbValeursNumeriques());
    }

    @Test
    void entier_1_listeEntiers() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        Integer[] attendu = {1};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeurs());
    }

    @Test
    void entier_99_listeEntiers() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("99");
        Integer[] attendu = {99};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeurs());
    }

    @Test
    void entier_1_listeEntiersTries() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        Integer[] attendu = {1};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeursTriees());
    }

    @Test
    void entier_99_listeEntiersTries() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("99");
        Integer[] attendu = {99};
        assertIterableEquals(Arrays.asList(attendu), test.listeDesValeursTriees());
    }

    @Test
    void entier_1_listeOpe() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("1");
        assertTrue(test.listeDesOperateurs().isEmpty());

    }

    @Test
    void entier_99_listeOpe() throws EvaluationImpossibleException {
        test = new NoeudValeurEntiere("99");
        assertTrue(test.listeDesOperateurs().isEmpty());
    }

}

