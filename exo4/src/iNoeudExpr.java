import java.util.List;

/**
 * Interface décrivant les fonctionnalités attendues par des Noeuds
 * constituant une expression arithmétique
 *
 * @param <N> le type de nombre considéré
 */

public interface iNoeudExpr<N extends Number> {

    /**
     * Accesseur du fils gauche.
     *
     * @return le fils gauche du noeud.
     * @throws PasDeFilsException si le noeud n'a pas de fils
     */
    iNoeudExpr<N> donneFilsG() throws PasDeFilsException;

    /**
     * Accesseur du fils droit.
     *
     * @return le fils droit du noeud.
     * @throws PasDeFilsException si le noeud n'a pas de fils
     */
    iNoeudExpr<N> donneFilsD() throws PasDeFilsException;

    /**
     * Méthode qui indique si le noeud possède un fils gauche.
     *
     * @return vrai si le noeud possède un fils gauche, faux sinon.
     */
    Boolean aFilsG();

    /**
     * Méthode qui indique si le noeud possède un fils droit.
     *
     * @return vrai si le noeud possède un fils droit, faux sinon.
     */
    Boolean aFilsD();

    /**
     * Méthode qui donne le nombre de valeurs numériques de l'expression
     *
     * @return le nombre de valeurs de l'expression
     */
    int nbValeursNumeriques();

    /**
     * Méthode pour calculer l'expression considérée
     * contient soit un opérateur, un fils gauche et un fils droit
     * soit une valeur et aucun fils gauche et droit.
     *
     * @return le résultat du calcul de l'expression considérée
     */
    N evalue();

    /**
     * Méthode qui produit une chaine de caractère représentant l'expression mathématique
     * sous sa forme correctement parenthesée (sans espace)
     *
     * @return une chaine de caractère
     */
    String affiche();

    /**
     * Méthode qui retourne la liste des valeurs numériques
     * dans leur ordre d'apparition dans l'expression considérée
     *
     * @return une liste de valeurs numériques
     */
    List<N> listeDesValeurs();

    /**
     * Méthode qui retourne la liste des opérateurs
     * dans leur ordre d'apparition dans l'expression considérée
     *
     * @return une liste de String
     */
    List<String> listeDesOperateurs();

    /**
     * Méthode qui retourne une liste ORDONNNEES et SANS DOUBLON
     * des valeurs numériques présentes dans l'expression considérée
     *
     * @return une liste de valeurs numériques
     */
    List<N> listeDesValeursTriees();
}
