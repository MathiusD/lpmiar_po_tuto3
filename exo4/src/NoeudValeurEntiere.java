import java.util.List;

/**
 * Classe concrete représentant un noeud "valeur" d'une expression artithmétique entière
 */

public class NoeudValeurEntiere implements iNoeudExpr<Integer> {

    protected Integer valeur;

    /**
     * Constructeur.
     *
     * @param str la valeur du noeud.
     * @throws EvaluationImpossibleException si la valeur str ne peut pas être correctement évaluée
     *                                       comme un entier
     */
    public NoeudValeurEntiere(String str)
            throws EvaluationImpossibleException {
        // TODO
    }


    @Override
    public iNoeudExpr<Integer> donneFilsG() throws PasDeFilsException {
        return null;
    }

    @Override
    public iNoeudExpr<Integer> donneFilsD() throws PasDeFilsException {
        return null;
    }

    @Override
    public Boolean aFilsG() {
        return null;
    }

    @Override
    public Boolean aFilsD() {
        return null;
    }

    @Override
    public int nbValeursNumeriques() {
        return 0;
    }

    @Override
    public Integer evalue() {
        return null;
    }

    @Override
    public String affiche() {
        return null;
    }

    @Override
    public List<Integer> listeDesValeurs() {
        return null;
    }

    @Override
    public List<String> listeDesOperateurs() {
        return null;
    }

    @Override
    public List<Integer> listeDesValeursTriees() {
        return null;
    }
}

