import java.util.List;

/**
 * Classe concrete représentant un noeud d'une expression arithmétique entiere :
 * les opérateurs valides sont '+', '-', '*' ou '/'
 */
public class NoeudOperateurEntier implements iNoeudExpr<Integer> {

    protected iNoeudExpr<Integer> filsG;
    protected String operateur;
    protected iNoeudExpr<Integer> filsD;

    /**
     * Constructeur.
     *
     * @param fg le fils gauche du noeud.
     * @param op l'opérateur du noeud.
     * @param fd le fils droit du noeud.
     * @throws PasDeFilsException            si l'un des fils n'est pas correctement initialisé (=null)
     * @throws EvaluationImpossibleException si l'operateur proposé n'est pas valide
     */
    public NoeudOperateurEntier(iNoeudExpr<Integer> fg, String op, iNoeudExpr<Integer> fd)
            throws PasDeFilsException, EvaluationImpossibleException {
        // TODO
    }


    private boolean valeurOK() {
        switch (operateur) {
            case "+":
                return true;
            case "-":
                return true;
            case "*":
                return true;
            case "/":
                return true;
            default:
                return false;
        }
    }

    @Override
    public iNoeudExpr<Integer> donneFilsG() throws PasDeFilsException {
        return null;
    }

    @Override
    public iNoeudExpr<Integer> donneFilsD() throws PasDeFilsException {
        return null;
    }

    @Override
    public Boolean aFilsG() {
        return null;
    }

    @Override
    public Boolean aFilsD() {
        return null;
    }

    @Override
    public int nbValeursNumeriques() {
        return 0;
    }

    @Override
    public Integer evalue() {
        return null;
    }

    @Override
    public String affiche() {
        return null;
    }

    @Override
    public List<Integer> listeDesValeurs() {
        return null;
    }

    @Override
    public List<String> listeDesOperateurs() {
        return null;
    }

    @Override
    public List<Integer> listeDesValeursTriees() {
        return null;
    }
}
