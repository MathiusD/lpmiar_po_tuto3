import java.util.List;

public interface Paques {


    /*Pâques est le dimanche qui suit la première pleine lune du printemps, c'est-à-dire, selon la définition établie par le Concile de Nicée en 325 :

            « Pâques est le dimanche qui suit le 14e jour de la Lune qui atteint cet âge le 21 mars ou immédiatement après. »

    Selon cette définition, Pâques tombe entre le 22 mars et le 25 avril de chaque année.
    */


    Date calculeDatePaques(int annee) throws PaquesException;

    List<Date> historiqueResultats();

    List<Date> historiqueResultatsTries();
}
