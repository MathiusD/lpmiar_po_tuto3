import java.util.List;

public class PaquesJulienne implements Paques {


    public PaquesJulienne() {
    }

    /*
    voir : https://fr.wikipedia.org/wiki/Calcul_de_la_date_de_P%C3%A2ques_selon_la_m%C3%A9thode_de_Meeus#Calcul_de_la_date_de_P%C3%A2ques_julienne
     */
    @Override
    public Date calculeDatePaques(int annee) throws PaquesException {
        if (annee < 326)
            throw new PaquesException();
        int a  = annee % 19;
        int b  = annee % 7;
        int c  = annee % 4;
        int d = (19 * a + 15) % 30;
        int e = (2 * c + 4 * b - d + 34) % 7;
        int prepGF = (d + e + 114);
        int g = prepGF % 31;
        int f = prepGF / 31;
        return new Date(g + 1, f, annee);
    }

    @Override
    public List<Date> historiqueResultats() {
        // TODO : question 3
        return null;
    }

    @Override
    public List<Date> historiqueResultatsTries() {
        // TODO : question 4
        return null;
    }
}
