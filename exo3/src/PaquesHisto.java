import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class PaquesHisto implements Paques {

    protected List<Date> resultats;

    public PaquesHisto() {
        resultats = new ArrayList<>();
    }

    @Override
    public List<Date> historiqueResultats() {
        return resultats;
    }

    @Override
    public List<Date> historiqueResultatsTries() {
        System.out.println(resultats);
        List<Date> copie = new LinkedList<>(resultats);
        copie.sort(null);
        System.out.println(copie);
        return copie;
    }
}
