import java.util.List;

public class PaquesGregorienne implements Paques {


    public PaquesGregorienne() {

    }

    /*
    voir : https://fr.wikipedia.org/wiki/Calcul_de_la_date_de_P%C3%A2ques_selon_la_m%C3%A9thode_de_Meeus#Calcul_de_la_date_de_P%C3%A2ques_gr%C3%A9gorienne
     */
    @Override
    public Date calculeDatePaques(int annee) throws PaquesException {
        if (annee < 1583)
            throw new PaquesException();
        int n = annee % 19;
        int c = annee / 100;
        int u = annee % 100;
        int s = c / 4;
        int t = c % 4;
        int p = (c + 8) / 25;
        int q = (c - p + 1) / 3;
        int e = (19 * n + c - s - q + 15) % 30;
        int b = u / 4;
        int d = u % 4;
        int L = (2 * t + 2 * b - e - d + 32) % 7;
        int h = (n + 11 * e + 22 * L) / 451;
        int prepMJ = e + L - 7 * h + 114;
        int m = prepMJ / 31;
        int j = prepMJ % 31;
        return new Date(m == 3 || m == 4 ? j + 1: j, m, annee);
    }


    @Override
    public List<Date> historiqueResultats() {
        // TODO : question 3
        return null;
    }

    @Override
    public List<Date> historiqueResultatsTries() {
        // TODO : question 4
        return null;
    }
}
