/**
 * Classe très simple représenant le concept d'une date
 */

public class Date {

    private int jour;
    private int mois;
    private int annee;

    /**
     * Constructeur définissant une date
     *
     * @param j entier représentant le jour
     * @param m entier représentant le mois
     * @param a entier représentant l'annee
     */
    public Date(int j, int m, int a) {
        jour = j;
        mois = m;
        annee = a;
    }

    /**
     * donne une chaine de caractères correspondant à la date
     *
     * @return une chaine représentant la date
     */
    @Override
    public String toString() {
        return "" + jour + "-" + mois + "-" + annee;
    }

    /**
     * methode qui permet de vérifier si une date est valide, cad si le jour, mois et année sont cohérent :
     * uniquement 28 jours en février, sauf les années bisxectiles, uniquement 30 jours en juin, etc.
     *
     * @return true si la date est valide
     */
    public boolean dateValide() {
        // TODO : question 6
        return false;
    }

    /**
     * fabrique une date valide à partir de la chaine de caractère passé en paramètre
     *
     * @param str chaine representant une date dans le format jour-mois-annee
     * @return la date correspondant à la chaine de caractère
     * @throws Exception si la chaine  est malformée
     */
    Date fabrique(String str) throws Exception {
        // TODO : question 7
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == this.getClass()) {
            return this.annee == ((Date) obj).annee && this.mois == ((Date) obj).mois && this.jour == ((Date) obj).jour;
        }
        return false;
    }
}
