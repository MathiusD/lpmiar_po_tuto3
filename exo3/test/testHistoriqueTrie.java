import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

public class testHistoriqueTrie {

    @Test
    public void testHistoTri() throws Exception {
        Paques paques = new PaquesGregorienne();
        List<Integer> entries = Arrays.asList(2022, 125, 1492, 2019, 2018, 1583, 2016, 1492, 23, 2017, 2002, 1582, 2018);
        Iterator<Integer> ite = entries.iterator();
        while (ite.hasNext()) {
            try {
                Date d = paques.calculeDatePaques(ite.next());
            } catch (Exception e) {
                //
            }
        }
        List<Date> expected = Arrays.asList(
                new Date(27, 3, 2016), new Date(31, 3, 2002), new Date(1, 4, 2018), new Date(1, 4, 2018),
                new Date(10, 4, 1583), new Date(16, 4, 2017), new Date(17, 4, 2022), new Date(21, 4, 2019));
        assertIterableEquals(expected, paques.historiqueResultatsTries());
    }


    @Test
    public void testHisto() throws Exception {
        Paques paques = new PaquesGregorienne();
        List<Integer> entries = Arrays.asList(2022, 125, 1492, 2019, 2018, 1583, 2016, 1492, 23, 2017, 2002, 1582, 2018);
        Iterator<Integer> ite = entries.iterator();
        while (ite.hasNext()) {
            try {
                Date d = paques.calculeDatePaques(ite.next());
            } catch (Exception e) {
                //
            }
        }

        paques.historiqueResultatsTries();

        List<Date> expected = Arrays.asList(
                new Date(17, 4, 2022), new Date(21, 4, 2019), new Date(1, 4, 2018), new Date(10, 4, 1583),
                new Date(27, 3, 2016), new Date(16, 4, 2017), new Date(31, 3, 2002), new Date(1, 4, 2018));
        assertIterableEquals(expected, paques.historiqueResultats());
    }


}
