import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestPaquesGregorienne {


    Paques p;

    @BeforeEach
    public void init() {
        p = new PaquesGregorienne();

    }

    @Test
    public void testPaquesGregorienne125() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(125));
    }

    @Test
    public void testPaquesJulienne325() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(325));
    }

    @Test
    public void testPaquesJulienne326() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(326));
    }

    @Test
    public void testPaquesJulienne327() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(327));
    }

    @Test
    public void testPaquesGregorienne1492() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(1492));
    }

    @Test
    public void testPaquesGregorienne1582() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(1582));
    }

    @Test
    public void testPaquesGregorienne1583() throws Exception {
        Date d = p.calculeDatePaques(1583);
        assertEquals(new Date(10, 4, 1583), d);
    }

    @Test
    public void testPaquesGregorienne2006() throws Exception {
        Date d = p.calculeDatePaques(2006);
        assertEquals(new Date(16, 4, 2006), d);
    }

    @Test
    public void testPaquesGregorienne2015() throws Exception {
        Date d = p.calculeDatePaques(2015);
        assertEquals(new Date(5, 4, 2015), d);
    }

    @Test
    public void testPaquesGregorienne2018() throws Exception {
        Date d = p.calculeDatePaques(2018);
        assertEquals(new Date(1, 4, 2018), d);
    }

    @Test
    public void testPaquesGregorienne2019() throws Exception {
        Date d = p.calculeDatePaques(2019);
        assertEquals(new Date(21, 4, 2019), d);
    }

    @Test
    public void testPaquesGregorienne2022() throws Exception {
        Date d = p.calculeDatePaques(2022);
        assertEquals(new Date(17, 4, 2022), d);
    }

    @Test
    public void testPaquesGregorienne2040() throws Exception {
        Date d = p.calculeDatePaques(2040);
        assertEquals(new Date(1, 4, 2040), d);
    }
}
