import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestPaquesJulienne {

    Paques p;

    @BeforeEach
    public void init() {
        p = new PaquesJulienne();

    }

    @Test
    public void testPaquesJulienne125() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(125));
    }

    @Test
    public void testPaquesJulienne324() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(324));
    }

    @Test
    public void testPaquesJulienne325() throws Exception {
        assertThrows(PaquesException.class,
                () -> p.calculeDatePaques(325));
    }

    @Test
    public void testPaquesJulienne326() throws Exception {
        Date d = p.calculeDatePaques(326);
        assertEquals("3-4-326", d.toString());
    }

    @Test
    public void testPaquesJulienne327() throws Exception {
        Date d = p.calculeDatePaques(327);
        assertEquals("26-3-327", d.toString());
    }

    @Test
    public void testPaquesJulienne1492() throws Exception {
        Date d = p.calculeDatePaques(1492);
        assertEquals("22-4-1492", d.toString());
    }


}
