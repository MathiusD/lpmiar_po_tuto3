# exo 3 : algorithmique

_Des algorithmes permettent de calculer la date de Pâques 
à partir de simples opérations sur les entiers :
https://fr.wikipedia.org/wiki/Calcul_de_la_date_de_P%C3%A2ques_selon_la_m%C3%A9thode_de_Meeus_

  
L'interface _Paques.java_ définit plusieurs fonctionnalités
 qu'il vous faudra implémenter.

1. Implémenter la méthode _calculeDatePaques()_ pour 
la classe _PaquesJulienne.java_ : [wikipedia "Paques julienne"](https://fr.wikipedia.org/wiki/Calcul_de_la_date_de_P%C3%A2ques_selon_la_m%C3%A9thode_de_Meeus#Calcul_de_la_date_de_P%C3%A2ques_julienne) ;
Des cas de tests dans _TestPaquesJulienne.java_ 
vous permettent de valider votre dev.
 
2. Implémenter la méthode _calculeDatePaques()_ pour 
la classe _PaquesGregorienne.java_ : [Wikipedia "Paques gregorienne"](https://fr.wikipedia.org/wiki/Calcul_de_la_date_de_P%C3%A2ques_selon_la_m%C3%A9thode_de_Meeus#Calcul_de_la_date_de_P%C3%A2ques_gr%C3%A9gorienne) ;
Des cas de tests dans _TestPaquesGregorienne.java_ 
vous permettent de valider votre dev.
 NB : pour que tous les tests passent il est sûrement nécessaire 
d'ajouter quelque chose à la classe Date.

3. Implémenter la méthode _historiqueResultats()_ ;
Des cas de tests dans _TestHistorique.java_ vous 
permettent de valider votre dev. NB : réfléchir à à éviter toute
duplication inutile du code.
 
4. Implémenter la méthode _historiqueResultatsTries()_
Des cas de tests dans _TestHistoriqueTrie.java_ 
vous permettent de valider votre dev. 
NB : éviter de faire les choses "à la main", mais utiliser 
tous les outils Java à votre disposition

5. Donner des cas de tests pour tester l'implémentation 
de la méthode _dateValide()_ dans la classe _Date.java_. 

6. Implémenter la méthode _dateValide()_.

7. Implémenter la méthode _fabrique(String str)_ 
dans la classe _Da